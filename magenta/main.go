package main

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"log"
	"net/http"
)

type Employee struct {
	gorm.Model
	Name   string `gorm:"unique" json:"name"`
	City   string `json:"city"`
	Age    int    `json:"age"`
	Status bool   `json:"status"`
}

var db *gorm.DB
var err error

// user:password@tcp(host:port)/dbname?charset=utf8&parseTime=True

func main() {
	db, err = gorm.Open("mysql", "root:@tcp(127.0.0.1:3306)/magenta?charset=utf8&parseTime=True")

	if err!=nil {
		log.Println("Connection Failed to Open")
	} else {
		log.Println("Connection Established")
	}

	db.AutoMigrate(&Employee{})

	// Server
	router := mux.NewRouter()
	router.HandleFunc("/", Home).Methods("GET")
	router.HandleFunc("/employees", CreateEmployee).Methods("POST")
	router.HandleFunc("/employees", GetEmployees).Methods("GET")
	router.HandleFunc("/employees/{name}", GetEmployeesByID).Methods("GET")
	log.Fatal(http.ListenAndServe(":8099", router))
}

func Home(w http.ResponseWriter, r *http.Request){
	w.Header().Set("Content-Type", "application/json")
	_ = json.NewEncoder(w).Encode("Welcome Home Bro!")
}

func CreateEmployee(w http.ResponseWriter, r *http.Request){
	employee := Employee{}
	decoder := json.NewDecoder(r.Body)

	if err := decoder.Decode(&employee); err != nil {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusBadRequest)
		_ = json.NewEncoder(w).Encode(err.Error())
		return
	}
	defer r.Body.Close()

	if errSave := db.Save(&employee).Error; errSave != nil {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		_ = json.NewEncoder(w).Encode(errSave)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	_ = json.NewEncoder(w).Encode(employee)
}

func GetEmployees(w http.ResponseWriter, r *http.Request){
	var employees []Employee

	if errGetAll := db.Find(&employees).Error; errGetAll != nil {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		_ = json.NewEncoder(w).Encode(errGetAll)
		return
	}

	if len(employees) <= 0 {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)

		errResponse := map[string]interface{}{
			"code" : "DATA_NOT_FOUND",
			"message" : "Data not found.",
			"data" : []map[string]interface{}{},
		}

		_ = json.NewEncoder(w).Encode(errResponse)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	_ = json.NewEncoder(w).Encode(employees)
}

func GetEmployeesByID(w http.ResponseWriter, r *http.Request){
	vars := mux.Vars(r)
	name := vars["name"]

	employee := Employee{}

	if err := db.Where(Employee{Name: name}).Find(&employee).Error; err != nil {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)

		errResponse := map[string]interface{}{
			"code" : "DATA_NOT_FOUND",
			"message" : "Data not found.",
			"data" : map[string]interface{}{},
		}

		_ = json.NewEncoder(w).Encode(errResponse)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	_ = json.NewEncoder(w).Encode(employee)
}