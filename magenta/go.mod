module magenta

go 1.13

require (
	github.com/gorilla/mux v1.6.2
	github.com/jinzhu/gorm v1.9.11
)
