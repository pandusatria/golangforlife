package handler

import (
	"fmt"

	"gitlab.com/pandusatria/golangforlife/introduction/model"
)

func Create() *model.Person {
	person := &model.Person{}
	dream := &model.Dream{}

	dream = &model.Dream{
		Type:        "Wet",
		Description: "Banana",
	}

	person.ID = 1
	person.Name = "Refqi"
	person.Age = 40
	person.Address = "Cakung"
	person.Dream = *dream

	fmt.Printf("Model Created : %v \n", *person)
	return person
}

func Edit(person *model.Person) {
	fmt.Printf("Model Before Edit : %v \n", *person)

	afterPerson := &model.Person{}

	afterPerson = &model.Person{
		ID:      person.ID,
		Name:    "Indra",
		Age:     person.Age,
		Address: "Malang",
		Dream:   person.Dream,
	}

	fmt.Printf("Model After Edit : %v \n", *afterPerson)
}
