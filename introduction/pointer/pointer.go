package pointer

import "fmt"

func Pointer() {
	var k string = "pandu"
	var l *string = &k

	fmt.Println("K (value) : ", k)
	fmt.Println("K (address) : ", &k)

	fmt.Println("L (value) : ", *l)
	fmt.Println("L (address) : ", l)

	k = "putra"

	fmt.Println("K1 (value) : ", k)
	fmt.Println("K1 (address) : ", &k)

	fmt.Println("L1 (value) : ", *l)
	fmt.Println("L1 (address) : ", l)
}
