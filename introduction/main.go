package main

import (
	"gitlab.com/pandusatria/golangforlife/introduction/handler"
	"gitlab.com/pandusatria/golangforlife/introduction/model"
)

func main() {
	/* var a string = "Fulan"
	b := 1
	cetak.Cetak(b, a)

	d := cetak.Indra("dana cukup", "diduain")
	fmt.Printf("%s \n", d)

	_, _, y, _ := cetak.Ady()
	fmt.Println("istri ady : " + y)

	s := cetak.Yana(1, 2, 3)
	fmt.Printf("rata-rata : %f \n", s)

	pointer.Pointer() */

	var createdPerson *model.Person
	createdPerson = handler.Create()

	handler.Edit(createdPerson)
}
